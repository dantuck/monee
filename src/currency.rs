mod iso;

pub use crate::Error;
pub use iso::Iso;
use std::collections::HashMap;
use std::fmt;

lazy_static! {
    static ref CURRENCIES_BY_ALPHA_CODE: HashMap<String, Currency> =
        Currency::generate_currencies_by_alpha_code();
}

#[derive(Debug, PartialEq, Eq)]
pub struct Currency {
    pub code: &'static str,
    pub name: &'static str,
    pub symbol: &'static str,
}

impl fmt::Display for Currency {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.code)
    }
}

impl Currency {
    pub fn find(code: &str) -> Result<&'static Currency, Error> {
        Currency::from_string(code.to_string())
    }

    pub fn from_string(code: String) -> Result<&'static Currency, Error> {
        Currency::find_by_alpha_iso(code).ok_or(Error::InvalidCurrency)
    }

    /// Returns a Currency given an alphabetic ISO-4217 currency code.
    pub fn find_by_alpha_iso(code: String) -> Option<&'static Currency> {
        match CURRENCIES_BY_ALPHA_CODE.get(&code.to_uppercase()) {
            Some(c) => Some(c),
            None => None,
        }
    }

    /// Returns a Currency Hashmap, keyed by ISO alphabetic code.
    fn generate_currencies_by_alpha_code() -> HashMap<String, Currency> {
        let mut num_map: HashMap<String, Currency> = HashMap::new();

        for item in iso::ISO_CURRENCIES {
            let currency = iso::from_enum(item);
            num_map.insert(currency.code.to_string(), currency);
        }
        num_map
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_CURRENCIES: [Currency; 170] = [
        Currency {
            code: "AED",
            name: "United Arab Emirates Dirham",
            symbol: "د.إ",
        },
        Currency {
            code: "AFN",
            name: "Afghan Afghani",
            symbol: "؋",
        },
        Currency {
            code: "AMD",
            name: "Armenian Dram",
            symbol: "դր.",
        },
        Currency {
            code: "ANG",
            name: "Netherlands Antillean Gulden",
            symbol: "ƒ",
        },
        Currency {
            code: "AOA",
            name: "Angolan Kwanza",
            symbol: "Kz",
        },
        Currency {
            code: "ARS",
            name: "Argentine Peso",
            symbol: "$",
        },
        Currency {
            code: "AUD",
            name: "Australian Dollar",
            symbol: "$",
        },
        Currency {
            code: "AWG",
            name: "Aruban Florin",
            symbol: "ƒ",
        },
        Currency {
            code: "AZN",
            name: "Azerbaijani Manat",
            symbol: "₼",
        },
        Currency {
            code: "BAM",
            name: "Bosnia and Herzegovina Convertible Mark",
            symbol: "KM",
        },
        Currency {
            code: "BBD",
            name: "Barbadian Dollar",
            symbol: "$",
        },
        Currency {
            code: "BDT",
            name: "Bangladeshi Taka",
            symbol: "৳",
        },
        Currency {
            code: "BGN",
            name: "Bulgarian Lev",
            symbol: "лв.",
        },
        Currency {
            code: "BHD",
            name: "Bahraini Dinar",
            symbol: "ب.د",
        },
        Currency {
            code: "BIF",
            name: "Burundian Franc",
            symbol: "Fr",
        },
        Currency {
            code: "BMD",
            name: "Bermudian Dollar",
            symbol: "$",
        },
        Currency {
            code: "BND",
            name: "Brunei Dollar",
            symbol: "$",
        },
        Currency {
            code: "BOB",
            name: "Bolivian Boliviano",
            symbol: "Bs.",
        },
        Currency {
            code: "BRL",
            name: "Brazilian real",
            symbol: "R$",
        },
        Currency {
            code: "BSD",
            name: "Bahamian Dollar",
            symbol: "$",
        },
        Currency {
            code: "BTN",
            name: "Bhutanese Ngultrum",
            symbol: "Nu.",
        },
        Currency {
            code: "BWP",
            name: "Botswana Pula",
            symbol: "P",
        },
        Currency {
            code: "BYN",
            name: "Belarusian Ruble",
            symbol: "Br",
        },
        Currency {
            code: "BYR",
            name: "Belarusian Ruble",
            symbol: "Br",
        },
        Currency {
            code: "BZD",
            name: "Belize Dollar",
            symbol: "$",
        },
        Currency {
            code: "CAD",
            name: "Canadian Dollar",
            symbol: "$",
        },
        Currency {
            code: "CDF",
            name: "Congolese Franc",
            symbol: "Fr",
        },
        Currency {
            code: "CHF",
            name: "Swiss Franc",
            symbol: "Fr",
        },
        Currency {
            code: "CLF",
            name: "Unidad de Fomento",
            symbol: "UF",
        },
        Currency {
            code: "CLP",
            name: "Chilean Peso",
            symbol: "$",
        },
        Currency {
            code: "CNY",
            name: "Chinese Renminbi Yuan",
            symbol: "¥",
        },
        Currency {
            code: "COP",
            name: "Colombian Peso",
            symbol: "$",
        },
        Currency {
            code: "CRC",
            name: "Costa Rican Colón",
            symbol: "₡",
        },
        Currency {
            code: "CUC",
            name: "Cuban Convertible Peso",
            symbol: "$",
        },
        Currency {
            code: "CUP",
            name: "Cuban Peso",
            symbol: "$",
        },
        Currency {
            code: "CVE",
            name: "Cape Verdean Escudo",
            symbol: "$",
        },
        Currency {
            code: "CZK",
            name: "Czech Koruna",
            symbol: "Kč",
        },
        Currency {
            code: "DJF",
            name: "Djiboutian Franc",
            symbol: "Fdj",
        },
        Currency {
            code: "DKK",
            name: "Danish Krone",
            symbol: "kr.",
        },
        Currency {
            code: "DOP",
            name: "Dominican Peso",
            symbol: "$",
        },
        Currency {
            code: "DZD",
            name: "Algerian Dinar",
            symbol: "د.ج",
        },
        Currency {
            code: "EGP",
            name: "Egyptian Pound",
            symbol: "ج.م",
        },
        Currency {
            code: "ERN",
            name: "Eritrean Nakfa",
            symbol: "Nfk",
        },
        Currency {
            code: "ETB",
            name: "Ethiopian Birr",
            symbol: "Br",
        },
        Currency {
            code: "EUR",
            name: "Euro",
            symbol: "€",
        },
        Currency {
            code: "FJD",
            name: "Fijian Dollar",
            symbol: "$",
        },
        Currency {
            code: "FKP",
            name: "Falkland Pound",
            symbol: "£",
        },
        Currency {
            code: "GBP",
            name: "British Pound",
            symbol: "£",
        },
        Currency {
            code: "GEL",
            name: "Georgian Lari",
            symbol: "ლ",
        },
        Currency {
            code: "GHS",
            name: "Ghanaian Cedi",
            symbol: "₵",
        },
        Currency {
            code: "GIP",
            name: "Gibraltar Pound",
            symbol: "£",
        },
        Currency {
            code: "GMD",
            name: "Gambian Dalasi",
            symbol: "D",
        },
        Currency {
            code: "GNF",
            name: "Guinean Franc",
            symbol: "Fr",
        },
        Currency {
            code: "GTQ",
            name: "Guatemalan Quetzal",
            symbol: "Q",
        },
        Currency {
            code: "GYD",
            name: "Guyanese Dollar",
            symbol: "$",
        },
        Currency {
            code: "HKD",
            name: "Hong Kong Dollar",
            symbol: "$",
        },
        Currency {
            code: "HNL",
            name: "Honduran Lempira",
            symbol: "L",
        },
        Currency {
            code: "HRK",
            name: "Croatian Kuna",
            symbol: "kn",
        },
        Currency {
            code: "HTG",
            name: "Haitian Gourde",
            symbol: "G",
        },
        Currency {
            code: "HUF",
            name: "Hungarian Forint",
            symbol: "Ft",
        },
        Currency {
            code: "IDR",
            name: "Indonesian Rupiah",
            symbol: "Rp",
        },
        Currency {
            code: "ILS",
            name: "Israeli New Sheqel",
            symbol: "₪",
        },
        Currency {
            code: "INR",
            name: "Indian Rupee",
            symbol: "₹",
        },
        Currency {
            code: "IQD",
            name: "Iraqi Dinar",
            symbol: "ع.د",
        },
        Currency {
            code: "IRR",
            name: "Iranian Rial",
            symbol: "﷼",
        },
        Currency {
            code: "ISK",
            name: "Icelandic Króna",
            symbol: "kr",
        },
        Currency {
            code: "JMD",
            name: "Jamaican Dollar",
            symbol: "$",
        },
        Currency {
            code: "JOD",
            name: "Jordanian Dinar",
            symbol: "د.ا",
        },
        Currency {
            code: "JPY",
            name: "Japanese Yen",
            symbol: "¥",
        },
        Currency {
            code: "KES",
            name: "Kenyan Shilling",
            symbol: "KSh",
        },
        Currency {
            code: "KGS",
            name: "Kyrgyzstani Som",
            symbol: "som",
        },
        Currency {
            code: "KHR",
            name: "Cambodian Riel",
            symbol: "៛",
        },
        Currency {
            code: "KMF",
            name: "Comorian Franc",
            symbol: "Fr",
        },
        Currency {
            code: "KPW",
            name: "North Korean Won",
            symbol: "₩",
        },
        Currency {
            code: "KRW",
            name: "South Korean Won",
            symbol: "₩",
        },
        Currency {
            code: "KWD",
            name: "Kuwaiti Dinar",
            symbol: "د.ك",
        },
        Currency {
            code: "KYD",
            name: "Cayman Islands Dollar",
            symbol: "$",
        },
        Currency {
            code: "KZT",
            name: "Kazakhstani Tenge",
            symbol: "₸",
        },
        Currency {
            code: "LAK",
            name: "Lao Kip",
            symbol: "₭",
        },
        Currency {
            code: "LBP",
            name: "Lebanese Pound",
            symbol: "ل.ل",
        },
        Currency {
            code: "LKR",
            name: "Sri Lankan Rupee",
            symbol: "₨",
        },
        Currency {
            code: "LRD",
            name: "Liberian Dollar",
            symbol: "$",
        },
        Currency {
            code: "LSL",
            name: "Lesotho Loti",
            symbol: "L",
        },
        Currency {
            code: "LYD",
            name: "Libyan Dinar",
            symbol: "ل.د",
        },
        Currency {
            code: "MAD",
            name: "Moroccan Dirham",
            symbol: "د.م.",
        },
        Currency {
            code: "MDL",
            name: "Moldovan Leu",
            symbol: "L",
        },
        Currency {
            code: "MGA",
            name: "Malagasy Ariary",
            symbol: "Ar",
        },
        Currency {
            code: "MKD",
            name: "Macedonian Denar",
            symbol: "ден",
        },
        Currency {
            code: "MMK",
            name: "Myanmar Kyat",
            symbol: "K",
        },
        Currency {
            code: "MNT",
            name: "Mongolian Tögrög",
            symbol: "₮",
        },
        Currency {
            code: "MOP",
            name: "Macanese Pataca",
            symbol: "P",
        },
        Currency {
            code: "MRU",
            name: "Mauritanian Ouguiya",
            symbol: "UM",
        },
        Currency {
            code: "MUR",
            name: "Mauritian Rupee",
            symbol: "₨",
        },
        Currency {
            code: "MVR",
            name: "Maldivian Rufiyaa",
            symbol: "MVR",
        },
        Currency {
            code: "MWK",
            name: "Malawian Kwacha",
            symbol: "MK",
        },
        Currency {
            code: "MXN",
            name: "Mexican Peso",
            symbol: "$",
        },
        Currency {
            code: "MYR",
            name: "Malaysian Ringgit",
            symbol: "RM",
        },
        Currency {
            code: "MZN",
            name: "Mozambican Metical",
            symbol: "MTn",
        },
        Currency {
            code: "NAD",
            name: "Namibian Dollar",
            symbol: "$",
        },
        Currency {
            code: "NGN",
            name: "Nigerian Naira",
            symbol: "₦",
        },
        Currency {
            code: "NIO",
            name: "Nicaraguan Córdoba",
            symbol: "C$",
        },
        Currency {
            code: "NOK",
            name: "Norwegian Krone",
            symbol: "kr",
        },
        Currency {
            code: "NPR",
            name: "Nepalese Rupee",
            symbol: "₨",
        },
        Currency {
            code: "NZD",
            name: "New Zealand Dollar",
            symbol: "$",
        },
        Currency {
            code: "OMR",
            name: "Omani Rial",
            symbol: "ر.ع.",
        },
        Currency {
            code: "PAB",
            name: "Panamanian Balboa",
            symbol: "B/.",
        },
        Currency {
            code: "PEN",
            name: "Peruvian Sol",
            symbol: "S/",
        },
        Currency {
            code: "PGK",
            name: "Papua New Guinean Kina",
            symbol: "K",
        },
        Currency {
            code: "PHP",
            name: "Philippine Peso",
            symbol: "₱",
        },
        Currency {
            code: "PKR",
            name: "Pakistani Rupee",
            symbol: "₨",
        },
        Currency {
            code: "PLN",
            name: "Polish Złoty",
            symbol: "zł",
        },
        Currency {
            code: "PYG",
            name: "Paraguayan Guaraní",
            symbol: "₲",
        },
        Currency {
            code: "QAR",
            name: "Qatari Riyal",
            symbol: "ر.ق",
        },
        Currency {
            code: "RON",
            name: "Romanian Leu",
            symbol: "ر.ق",
        },
        Currency {
            code: "RSD",
            name: "Serbian Dinar",
            symbol: "РСД",
        },
        Currency {
            code: "RUB",
            name: "Russian Ruble",
            symbol: "₽",
        },
        Currency {
            code: "RWF",
            name: "Rwandan Franc",
            symbol: "FRw",
        },
        Currency {
            code: "SAR",
            name: "Saudi Riyal",
            symbol: "ر.س",
        },
        Currency {
            code: "SBD",
            name: "Solomon Islands Dollar",
            symbol: "$",
        },
        Currency {
            code: "SCR",
            name: "Seychellois Rupee",
            symbol: "₨",
        },
        Currency {
            code: "SDG",
            name: "Sudanese Pound",
            symbol: "£",
        },
        Currency {
            code: "SEK",
            name: "Swedish Krona",
            symbol: "kr",
        },
        Currency {
            code: "SGD",
            name: "Singapore Dollar",
            symbol: "$",
        },
        Currency {
            code: "SHP",
            name: "Saint Helenian Pound",
            symbol: "£",
        },
        Currency {
            code: "SKK",
            name: "Slovak Koruna",
            symbol: "Sk",
        },
        Currency {
            code: "SLL",
            name: "Sierra Leonean Leone",
            symbol: "Le",
        },
        Currency {
            code: "SOS",
            name: "Somali Shilling",
            symbol: "Sh",
        },
        Currency {
            code: "SRD",
            name: "Surinamese Dollar",
            symbol: "$",
        },
        Currency {
            code: "SSP",
            name: "South Sudanese Pound",
            symbol: "£",
        },
        Currency {
            code: "STD",
            name: "São Tomé and Príncipe Dobra",
            symbol: "Db",
        },
        Currency {
            code: "SVC",
            name: "Salvadoran Colón",
            symbol: "₡",
        },
        Currency {
            code: "SYP",
            name: "Syrian Pound",
            symbol: "£S",
        },
        Currency {
            code: "SZL",
            name: "Swazi Lilangeni",
            symbol: "E",
        },
        Currency {
            code: "THB",
            name: "Thai Baht",
            symbol: "฿",
        },
        Currency {
            code: "TJS",
            name: "Tajikistani Somoni",
            symbol: "ЅМ",
        },
        Currency {
            code: "TMT",
            name: "Turkmenistani Manat",
            symbol: "T",
        },
        Currency {
            code: "TND",
            name: "Tunisian Dinar",
            symbol: "د.ت",
        },
        Currency {
            code: "TOP",
            name: "Tongan Paʻanga",
            symbol: "T$",
        },
        Currency {
            code: "TRY",
            name: "Turkish Lira",
            symbol: "₺",
        },
        Currency {
            code: "TTD",
            name: "Trinidad and Tobago Dollar",
            symbol: "$",
        },
        Currency {
            code: "TWD",
            name: "New Taiwan Dollar",
            symbol: "$",
        },
        Currency {
            code: "TZS",
            name: "Tanzanian Shilling",
            symbol: "Sh",
        },
        Currency {
            code: "UAH",
            name: "Ukrainian Hryvnia",
            symbol: "₴",
        },
        Currency {
            code: "UGX",
            name: "Ugandan Shilling",
            symbol: "USh",
        },
        Currency {
            code: "USD",
            name: "United States Dollar",
            symbol: "$",
        },
        Currency {
            code: "UYU",
            name: "Uruguayan Peso",
            symbol: "$",
        },
        Currency {
            code: "UZS",
            name: "Uzbekistan Som",
            symbol: "so'm",
        },
        Currency {
            code: "VES",
            name: "Venezuelan Bolívar Soberano",
            symbol: "Bs",
        },
        Currency {
            code: "VND",
            name: "Vietnamese Đồng",
            symbol: "₫",
        },
        Currency {
            code: "VUV",
            name: "Vanuatu Vatu",
            symbol: "Vt",
        },
        Currency {
            code: "WST",
            name: "Samoan Tala",
            symbol: "T",
        },
        Currency {
            code: "XAF",
            name: "Central African Cfa Franc",
            symbol: "CFA",
        },
        Currency {
            code: "XAG",
            name: "Silver (Troy Ounce)",
            symbol: "oz t",
        },
        Currency {
            code: "XAU",
            name: "Gold (Troy Ounce)",
            symbol: "oz t",
        },
        Currency {
            code: "XBA",
            name: "European Composite Unit",
            symbol: "",
        },
        Currency {
            code: "XBB",
            name: "European Monetary Unit",
            symbol: "",
        },
        Currency {
            code: "XBC",
            name: "European Unit of Account 9",
            symbol: "",
        },
        Currency {
            code: "XBD",
            name: "European Unit of Account 17",
            symbol: "",
        },
        Currency {
            code: "XCD",
            name: "East Caribbean Dollar",
            symbol: "$",
        },
        Currency {
            code: "XDR",
            name: "Special Drawing Rights",
            symbol: "SDR",
        },
        Currency {
            code: "XOF",
            name: "West African Cfa Franc",
            symbol: "Fr",
        },
        Currency {
            code: "XPD",
            name: "Palladium",
            symbol: "oz t",
        },
        Currency {
            code: "XPF",
            name: "Cfp Franc",
            symbol: "Fr",
        },
        Currency {
            code: "XPT",
            name: "Platinum",
            symbol: "oz t",
        },
        Currency {
            code: "XTS",
            name: "Codes specifically reserved for testing purposes",
            symbol: "oz t",
        },
        Currency {
            code: "YER",
            name: "Yemeni Rial",
            symbol: "﷼",
        },
        Currency {
            code: "ZAR",
            name: "South African Rand",
            symbol: "R",
        },
        Currency {
            code: "ZMK",
            name: "Zambian Kwacha",
            symbol: "ZK",
        },
        Currency {
            code: "ZMW",
            name: "Zambian Kwacha",
            symbol: "K",
        },
        Currency {
            code: "ZWL",
            name: "Zimbabwe Dollar",
            symbol: "Z$",
        },
    ];

    #[test]
    fn currency_find_known_can_be_found() {
        TEST_CURRENCIES.iter().for_each(|expected| {
            let actual = Currency::from_string(expected.code.to_string()).unwrap();
            assert_eq!(actual, expected);
        });
    }
    #[test]
    fn currency_from_string() {
        TEST_CURRENCIES.iter().for_each(|expected| {
            let actual = Currency::find(expected.code).unwrap();
            assert_eq!(actual, expected);
        });
    }

    #[test]
    fn currency_find_unknown_code_raise_invalid_currency_error() {
        assert_eq!(Currency::find("AAA").unwrap_err(), Error::InvalidCurrency,);
    }
}
